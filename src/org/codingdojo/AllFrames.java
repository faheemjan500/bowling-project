/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package org.codingdojo;

public class AllFrames {
  Frame head = null;

  public void convertToFrames(String[] allTurns) {
    for (final String singleTurn : allTurns) {
      if (singleTurn.length() == 1) {
        final Frame newFrame = new Frame(singleTurn, "-");
        addToQueue(newFrame);
      } else {
        final Frame newFrame =
            new Frame(String.valueOf(singleTurn.charAt(0)), String.valueOf(singleTurn.charAt(1)));
        addToQueue(newFrame);
      }
    }
  }

  public Frame getHeadFrame() {
    return this.head;
  }

  public int resultOfTheGame() {
    int sum = 0;
    Frame current = this.getHeadFrame();
    for (int i = 0; i < 10; i++) {
      sum = sum + current.getScore();
      current = current.getNext();
    }
    return sum;
  }

  private void addToQueue(Frame frame) {
    Frame current;
    if (this.head == null) {
      this.head = frame;
      this.head.setNext(null);
      this.setHead(this.head);
    } else {
      current = head;
      while (current.getNext() != null) {
        current = current.getNext();
      }
      if (current.getNext() == null) {
        current.setNext(frame);
      }
    }
  }

  private void setHead(Frame head) {
    this.head = head;
  }

}
