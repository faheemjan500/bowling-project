/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package org.codingdojo;

public class Frame {
  private String throw1;
  private String throw2;
  private Frame next;
  private int score;

  public Frame(String throw1, String throw2) {
    if (throw1.equals("-")) {
      this.throw1 = "0";
    } else {
      this.throw1 = throw1;
    }
    if (throw2.equals("-")) {
      this.throw2 = "0";
    } else {
      this.throw2 = throw2;
    }
  }

  public void caculateFrameScore() {
    if (Turn.isStrike(this)) {
      this.score = 10 + resultOfSumOfNextTwoThrows(this.getNext());
    } else if (Turn.isSpear(this)) {
      this.score = 10 + resultOfNextThrow(this.getNext());
    } else {
      this.score = Integer.parseInt(this.getThrow1()) + Integer.parseInt(this.getThrow2()); // if
    }
  }

  public Frame getNext() {
    return next;
  }

  public int getScore() {
    this.caculateFrameScore();
    return this.score;
  }

  public String getThrow1() {
    return throw1;
  }

  public String getThrow2() {
    return throw2;
  }

  public void setNext(Frame next) {
    this.next = next;
  }

  private int resultOfNextThrow(Frame frame) {
    if (Turn.isStrike(frame))
      return 10;
    else {
      return Integer.parseInt(frame.getThrow1());
    }
  }

  private int resultOfSumOfNextTwoThrows(Frame frame) {
    if (Turn.isStrike(frame)) {
      return 10 + resultOfNextThrow(frame.getNext());
    } else if (Turn.isSpear(frame)) {
      return 10;
    } else {
      return Integer.parseInt(frame.getThrow1()) + Integer.parseInt(frame.getThrow2()); // if fails.
    }
  }

}
