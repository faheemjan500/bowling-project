/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package org.codingdojo;

public class Turn {

  public static boolean ifFails(Frame frame) {
    return !frame.getThrow1().equals("X") && !frame.getThrow2().equals("/");
  }

  public static boolean isSpear(Frame frame) {
    return frame.getThrow2().equals("/");
  }

  public static boolean isStrike(Frame frame) {
    return frame.getThrow1().equals("X");
  }

  private Turn() {

  }

}
