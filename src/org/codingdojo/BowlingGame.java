/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package org.codingdojo;

public interface BowlingGame {

  public int frameScore(Frame frame);

  public int resultOfTheGame();

}
