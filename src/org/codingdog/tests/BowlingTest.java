/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package org.codingdog.tests;

import org.codingdojo.AllFrames;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BowlingTest {
  String[] input1;
  String[] input2;
  String[] input3;
  String[] input4;

  @Before
  public void beforeBowlingTest() {
    input1 = new String[] {
        "9-", "9-", "9-", "9-", "9-", "9-", "9-", "9-", "9-", "9-"
    };

    input2 = new String[] {
        "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X", "X"
    };
    input3 = new String[] {
        "5/", "5/", "5/", "5/", "5/", "5/", "5/", "5/", "5/", "5/", "5"
    };
    input4 = new String[] {
        "12", "-3", "44", "12", "12", "11", "12", "17", "81", "33"
    };
  }

  @Test // 3
  public void totalScoreShouldBe150ForInput3() {
    final AllFrames allFrames = new AllFrames();
    allFrames.convertToFrames(input3);
    final int actual = allFrames.resultOfTheGame();
    assertEquals(150, actual);
  }

  @Test // 2
  public void totalScoreShouldBe300ForInput2() {
    final AllFrames allFrames = new AllFrames();
    allFrames.convertToFrames(input2);
    final int actual = allFrames.resultOfTheGame();
    assertEquals(300, actual);
  }

  @Test // 4
  public void totalScoreShouldBe48ForInput4() {
    final AllFrames allFrames = new AllFrames();
    allFrames.convertToFrames(input4);
    final int actual = allFrames.resultOfTheGame();
    assertEquals(48, actual);
  }

  @Test // 1
  public void totalScoreShouldBe90ForInput1() {
    final AllFrames allFrames = new AllFrames();
    allFrames.convertToFrames(input1);
    final int actual = allFrames.resultOfTheGame();
    assertEquals(90, actual);
  }
}
