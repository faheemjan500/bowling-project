/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package org.codingdog.tests;

import org.codingdojo.Turn;
import org.codingdojo.Frame;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FrameTest {

  Frame frame1;
  Frame frame2;
  Frame frame3;
  Frame frame4;
  Frame frame5;

  @Before
  public void beforeBowlingTest() {
    frame1 = new Frame("2", "5");
    frame2 = new Frame("-", "/");
    frame3 = new Frame("-", "-");
    frame4 = new Frame("3", "4");
    frame5 = new Frame("X", "-");

  }

  @Test // 3
  public void itShouldFailWhenNotAllPinsAreDown() {
    final boolean expected = Turn.ifFails(frame1);
    assertTrue(expected);
  }

  @Test // 2
  public void shouldBeSpear() {
    frame2.setNext(frame4);
    final int actual = frame2.getScore();
    assertEquals(13, actual);
  }

  @Test // 5
  public void shouldStrikeWhenFirstTryIsX() {
    frame5.setNext(frame3);
    final int actual = frame5.getScore();
    assertEquals(10, actual);
  }

  @Test // 1
  public void sumShouldBe7() {
    final int actual = frame1.getScore();
    assertEquals(7, actual);
  }

  @Test // 4
  public void sumShouldBeZeroWhenNoPinDownInBothTries() {
    final int actual = frame3.getScore();
    assertEquals(0, actual);
  }

}
